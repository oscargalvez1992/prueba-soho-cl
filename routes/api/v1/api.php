<?php

$router->group(['prefix' => 'api/v1'], function () use ($router) {
    $router->post('auth/login', 'Auth\V1\AuthLoginController@login');
    $router->post('auth/register', 'Auth\V1\AuthRegisterController@register');

    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->post('auth/refresh', 'Auth\V1\AuthRefreshController@refresh');
        $router->post('auth/logout', 'Auth\V1\AuthLogoutController@logout');
        $router->get('users', 'User\V1\UserIndexController@index');
        $router->post('users', 'User\V1\UserStoreController@store');
        $router->put('users', 'User\V1\UserUpdateController@update');
        $router->patch('users/password', 'User\V1\UserUpdatePasswordController@update');
        $router->patch('users/status', 'User\V1\UserChangeStatusController@update');
    });
});
