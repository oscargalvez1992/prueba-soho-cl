<?php

namespace App\Http\Controllers\User\V1;

use App\Http\Controllers\Controller;
use App\Http\Repositories\User\UserIndexRepository;
use Illuminate\Http\JsonResponse;

class UserIndexController extends Controller
{
    /**
     * @var $userIndexRepository
     */
    protected $userIndexRepository;

    /**
     * __construct
     *
     * @param UserIndexRepository $userIndexRepository
     */
    public function __construct(UserIndexRepository $userIndexRepository) {
        $this->userIndexRepository = $userIndexRepository;
    }

    /**
     *
     * @OA\Get(
     *     path="/api/v1/users",
     *     tags={"User"},
     *     summary="List all User's",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="Logout Success",
     *         @OA\JsonContent(
     *               example={"status":"Success","code":200,"messages":{},"result":{{"id":1,"name":"admin","email":"admin@admin.com","status":1,"created_at":"2022-01-07T14:49:08.000000Z","updated_at":"2022-01-07T14:49:08.000000Z"},{"id":2,"name":"Oscar Galvez","email":"oscargalvez1992@gmail.com","status":1,"created_at":"2022-01-07T15:35:06.000000Z","updated_at":"2022-01-07T15:35:06.000000Z"}}},
     *          ),
     *     ),
     * )
     */

    /**
     * index
     * List all User's
     * @return JsonResponse
     */
    public function index() : JsonResponse
    {
       return $this->userIndexRepository->index();
    }

}
