<?php

namespace App\Http\Controllers\User\V1;


use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Repositories\User\UserStoreRepository;
use App\Http\Requests\User\V1\UserStoreRequest;

class UserStoreController extends Controller
{
    /**
     * @var $userStoreRepository
     */
    protected $userStoreRepository;

    /**
     * __construct
     *
     * @param UserStoreRepository $userStoreRepository
     */
    public function __construct(UserStoreRepository $userStoreRepository) {
        $this->userStoreRepository = $userStoreRepository;
    }

    /**
     * @OA\Post(
     *     path="/api/v1/users",
     *     tags={"User"},
     *     summary="Create User",
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password_confirmation",
     *                     type="string"
     *                 ),
     *
     *                 example={"name" : "Oscar Galvez","email" : "oscargalvez1992@gmail.com","password" : "Mecanica2022@@","password_confirmation" : "Mecanica2022@@"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Create Success",
     *         @OA\JsonContent(
     *               example={"status":"Success","code":201,"messages":{},"result":{"name":"Oscar Galvez","email":"oscargalvez19922@gmail.com","updated_at":"2022-01-07T17:59:13.000000Z","created_at":"2022-01-07T17:59:13.000000Z","id":4}},
     *          ),
     *
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error Validate",
     *         @OA\JsonContent(
     *               example={"message":"The given data was invalid.","errors":{"email":{"The email has already been taken."},"password":{"The password confirmation does not match."}}},
     *
     *          ),
     *
     *     ),
     * )
     */

    /**
     * store
     *
     * @param UserStoreRequest $request
     * @return JsonResponse
     */
    public function store(UserStoreRequest $request): JsonResponse
    {
        dd($request);
        return $this->userStoreRepository->store($request);
    }
}
