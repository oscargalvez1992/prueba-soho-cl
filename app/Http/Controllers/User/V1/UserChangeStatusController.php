<?php

namespace App\Http\Controllers\User\V1;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\V1\UserChangeStatusRequest;
use App\Http\Repositories\User\UserChangeStatusRepository;

class UserChangeStatusController extends Controller
{
    /**
     * @var $userStoreRepository
     */
    protected $userUpdatePasswordRepository;

    /**
     * __construct
     *
     * @param UserStoreRepository $userStoreRepository
     */
    public function __construct(UserChangeStatusRepository $userChangeStatusRepository) {
        $this->userChangeStatusRepository = $userChangeStatusRepository;
    }

    /**
     * @OA\Patch(
     *     path="/api/v1/users/status",
     *     tags={"User"},
     *     summary="Update Status User",
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="user_id",
     *                     type="number"
     *                 ),
     *                 @OA\Property(
     *                     property="status",
     *                     type="number"
     *                 ),
     *                 example={"user_id" : 2,"status" : 0}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Change Status Success",
     *         @OA\JsonContent(
     *               example={"status":"Success","code":201,"messages":{},"result":{"id":2,"name":"Oscar Galvez","email":"oscargalvez19922_mod@gmail.com","status":0,"created_at":"2022-01-07T15:35:06.000000Z","updated_at":"2022-01-07T21:55:41.000000Z"}},
     *          ),
     *
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error Validate",
     *         @OA\JsonContent(
     *               example={"message":"The given data was invalid.","errors":{"user_id":{"The user id field is required."},"status":{"The status field is required."}}},
     *
     *         ),
     *     ),
     * )
     */

    /**
     * store
     *
     * @param UserStoreRequest $request
     * @return JsonResponse
     */
    public function update(UserChangeStatusRequest $request): JsonResponse
    {
        return $this->userChangeStatusRepository->updateStatus($request);
    }
}
