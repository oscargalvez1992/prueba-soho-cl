<?php

namespace App\Http\Controllers\User\V1;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\V1\UserUpdatePasswordRequest;
use App\Http\Repositories\User\UserUpdatePasswordRepository;

class UserUpdatePasswordController extends Controller
{
    /**
     * @var $userStoreRepository
     */
    protected $userUpdatePasswordRepository;

    /**
     * __construct
     *
     * @param UserStoreRepository $userStoreRepository
     */
    public function __construct(UserUpdatePasswordRepository $userUpdatePasswordRepository) {
        $this->userUpdatePasswordRepository = $userUpdatePasswordRepository;
    }

    /**
     * @OA\Patch(
     *     path="/api/v1/users/password",
     *     tags={"User"},
     *     summary="Update Password User",
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="user_id",
     *                     type="number"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password_confirmation",
     *                     type="string"
     *                 ),
     *                 example={"user_id" : "2","password" : "Mecanica2022@@","password_confirmation" : "Mecanica2022@@"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Create Success",
     *         @OA\JsonContent(
     *               example={"status":"Success","code":201,"messages":{},"result":{"id":2,"name":"Oscar Galvez","email":"oscargalvez19922_mod@gmail.com","status":1,"created_at":"2022-01-07T15:35:06.000000Z","updated_at":"2022-01-07T21:27:10.000000Z"}},
     *          ),
     *
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error Validate",
     *         @OA\JsonContent(
     *               example={"message":"The given data was invalid.","errors":{"email":{"The email has already been taken."},"password":{"The password confirmation does not match."}}},
     *
     *          ),
     *
     *     ),
     * )
     */

    /**
     * store
     *
     * @param UserStoreRequest $request
     * @return JsonResponse
     */
    public function update(UserUpdatePasswordRequest $request): JsonResponse
    {
        return $this->userUpdatePasswordRepository->updatePassword($request);
    }
}
