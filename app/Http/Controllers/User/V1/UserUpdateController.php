<?php

namespace App\Http\Controllers\User\V1;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\V1\UserUpdateRequest;
use App\Http\Repositories\User\UserUpdateRepository;

class UserUpdateController extends Controller
{
    /**
     * @var $userStoreRepository
     */
    protected $userUpdateRepository;

    /**
     * __construct
     *
     * @param UserStoreRepository $userStoreRepository
     */
    public function __construct(UserUpdateRepository $userUpdateRepository) {
        $this->userUpdateRepository = $userUpdateRepository;
    }

    /**
     * @OA\Put(
     *     path="/api/v1/users",
     *     tags={"User"},
     *     summary="Update User",
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="user_id",
     *                     type="number"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                 example={"user_id" : 1,"name" : "Oscar Galvez","email" : "oscargalvez19922_mod@gmail.com"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Create Success",
     *         @OA\JsonContent(
     *               example={"status":"Success","code":201,"messages":{},"result":{"id":2,"name":"Oscar Galvez","email":"oscargalvez19922_mod@gmail.com","status":1,"created_at":"2022-01-07T15:35:06.000000Z","updated_at":"2022-01-07T20:54:52.000000Z"}},
     *          ),
     *
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error Validate",
     *         @OA\JsonContent(
     *               example={"message":"The given data was invalid.","errors":{"email":{"The email has already been taken."},"password":{"The password confirmation does not match."}}},
     *
     *          ),
     *
     *     ),
     * )
     */

    /**
     * store
     *
     * @param UserStoreRequest $request
     * @return JsonResponse
     */
    public function update(UserUpdateRequest $request): JsonResponse
    {
        return $this->userUpdateRepository->update($request);
    }
}
