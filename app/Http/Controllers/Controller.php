<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *   title="Prueba Soho CL",
 *   version="1.0",
 *   @OA\Contact(
 *     email="oscargalvez1992@gmail.com",
 *     name="Ingeniero Oscar Galvez"
 *   )
 * )
 */

/**
 * @OA\SecurityScheme(
 *    securityScheme="bearerAuth",
 *    in="header",
 *    name="bearerAuth",
 *    type="http",
 *    scheme="bearer",
 *    bearerFormat="JWT",
 * ),
**/
class Controller extends BaseController
{
    //
}

