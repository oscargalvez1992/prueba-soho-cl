<?php

namespace App\Http\Controllers\Auth\V1;

use App\Http\Repositories\Auth\AuthRegisterRepository;
use Illuminate\Http\JsonResponse;
use App\Http\Repositories\Auth\AuthRepository;
use App\Http\Requests\Auth\V1\RegisterRequest;

class AuthRegisterController extends AuthLoginController
{
    /**
    * @var $authRepository
    */
    protected $authRepository;

    /**
     * __construct
     *
     * @param AuthRepository $authRepository
     */
    public function __construct(AuthRegisterRepository $authRegisterRepository) {
        $this->authRegisterRepository = $authRegisterRepository;
    }

    /**
     * @OA\Post(
     *     path="/api/v1/auth/register",
     *     tags={"Auth"},
     *     summary="Register User Auth JWT",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password_confirmation",
     *                     type="string"
     *                 ),
     *
     *                 example={"name" : "Oscar Galvez","email" : "oscargalvez1992@gmail.com","password" : "Mecanica2022@@","password_confirmation" : "Mecanica2022@@"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Register Success",
     *         @OA\JsonContent(
     *               example={"status":"Success","code":200,"messages":{},"result":{"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC92MVwvYXV0aFwvcmVnaXN0ZXIiLCJpYXQiOjE2NDE1Njk3MDYsImV4cCI6MTY0MTU3MzMwNiwibmJmIjoxNjQxNTY5NzA2LCJqdGkiOiJ4NmRzQXRoR0RhQXpvOVh3Iiwic3ViIjoyLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.HVDpgqBCLdm-iznGog5_qM43iqx6jgCxEih41D60mCw","token_type":"bearer","expires_in":3600,"user":{"id":2,"name":"Oscar Galvez","email":"oscargalvez1992@gmail.com","status":1,"created_at":"2022-01-07T15:35:06.000000Z","updated_at":"2022-01-07T15:35:06.000000Z"}}},
     *          ),
     *
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error Validate",
     *         @OA\JsonContent(
     *               example={"message":"The given data was invalid.","errors":{"email":{"The email has already been taken."},"password":{"The password confirmation does not match."}}},
     *
     *          ),
     *
     *     ),
     * )
     */

    /**
     * register
     *
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        return $this->authRegisterRepository->register($request);
    }
}
