<?php

namespace App\Http\Controllers\Auth\V1;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Repositories\Auth\AuthRepository;
use App\Http\Repositories\Auth\AuthLogoutRepository;

class AuthLogoutController extends Controller
{
    /**
    * @var $authRepository
    */
    protected $authRepository;

    /**
     * __construct
     *
     * @param AuthRepository $authRepository
     */
    public function __construct(AuthLogoutRepository $authLogoutRepository) {
        $this->authLogoutRepository = $authLogoutRepository;
    }

    /**
     *
     * @OA\Post(
     *     path="/api/v1/auth/logout",
     *     tags={"Auth"},
     *     summary="Logout User Auth JWT",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="Logout Success",
     *         @OA\JsonContent(
     *               example={"status":"Success","code":200,"messages":{},"result":{"message":"Successfully logged out"}},
     *          ),
     *     ),
     * )
     */

    /**
     * logout
     *
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        return $this->authLogoutRepository->logout();
    }
}
