<?php

namespace App\Http\Controllers\Auth\V1;

use App\Traits\ApiResponser;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\V1\LoginRequest;
use App\Http\Repositories\Auth\AuthLoginRepository;

class AuthLoginController extends Controller
{
    use ApiResponser;

    /**
     * @var $authRepository
     */
    protected $AuthLoginRepository;

    /**
     * __construct
     *
     * @param AuthRepository $authRepository
     */
    public function __construct(AuthLoginRepository $authLoginRepository) {
        $this->authLoginRepository = $authLoginRepository;
    }

    /**
     * @OA\Post(
     *     path="/api/v1/auth/login",
     *     tags={"Auth"},
     *     summary="Login User Auth JWT",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *
     *                 example={"email" : "admin@admin.com","password" : "1234aA!64"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Login Success",
     *         @OA\JsonContent(
     *               example={"status":"Success","code":200,"messages":{},"result":{"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC92MVwvYXV0aFwvbG9naW4iLCJpYXQiOjE2NDE1Njc3MzMsImV4cCI6MTY0MTU3MTMzMywibmJmIjoxNjQxNTY3NzMzLCJqdGkiOiJRWUQwejRtVFZnT0EyZFRrIiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.qRFaZrV2ZvLqybBviLv7wML5oZ32VEKNC3vektlfukw","token_type":"bearer","expires_in":3600,"user":{"id":1,"name":"admin","email":"admin@admin.com","status":1,"created_at":"2022-01-07T14:49:08.000000Z","updated_at":"2022-01-07T14:49:08.000000Z"}}},
     *          ),
     *
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="User Inactive",
     *         @OA\JsonContent(
     *               example={"status":"Error","code":401,"messages":"User not active","result":{}},
     *
     *          ),
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Request Invalid All Fields Requiered",
     *         @OA\JsonContent(
     *               example={"message":"The given data was invalid.","errors":{"email":{"The email field is required."},"password":{"The password field is required."}}},
     *
     *          ),
     *
     *     ),
     *
     * )
     *
     */

    /**
     * login
     *
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        return $this->authLoginRepository->login($request);
    }
}
