<?php

namespace App\Http\Controllers\Auth\V1;


use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Repositories\Auth\AuthRefreshRepository;

class AuthRefreshController extends Controller
{

    /**
    * @var $authRepository
    */
    protected $authRepository;

    /**
     * __construct
     *
     * @param AuthRepository $authRepository
     */
    public function __construct(AuthRefreshRepository $authRefreshRepository) {
        $this->authRefreshRepository = $authRefreshRepository;
    }

    /**
     * @OA\Post(
     *     path="/api/v1/auth/refresh",
     *     tags={"Auth"},
     *     summary="Refresh Token User Auth JWT",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="Refresh Success",
     *         @OA\JsonContent(
     *               example={"status":"Success","code":200,"messages":{},"result":{"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC92MVwvYXV0aFwvcmVmcmVzaCIsImlhdCI6MTY0MTU2OTMxMCwiZXhwIjoxNjQxNTcyOTI5LCJuYmYiOjE2NDE1NjkzMjksImp0aSI6ImlUZzl5bEZqaFA4UmsyMHciLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.Q11IV-4bJTWyY7Pic3VeO9ULLAS5w1O7E0cCfu074Uc","token_type":"bearer","expires_in":3600,"user":{"id":1,"name":"admin","email":"admin@admin.com","status":1,"created_at":"2022-01-07T14:49:08.000000Z","updated_at":"2022-01-07T14:49:08.000000Z"}}},
     *          ),
     *     ),
     * )
     */

    /**
     * refresh
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh(): JsonResponse
    {
        return $this->authRefreshRepository->refresh();
    }
}
