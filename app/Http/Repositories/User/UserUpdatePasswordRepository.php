<?php

namespace App\Http\Repositories\User;

use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\User\V1\UserUpdatePasswordRequest;

class UserUpdatePasswordRepository extends UserRepository
{
    /**
     * @var $user
     */
    protected $user;

    /**
     * __construct
     *
     * @param User $user
     */
    public function __construct(User $user) {
        $this->user = $user;
    }

    /**
     * update
     *

     * @param UserUpdateRequest $request
     * @return JsonResponse
     */
    public function updatePassword(UserUpdatePasswordRequest $request) : JsonResponse
    {
        $user = $this->user::find($request->user_id);
        if ($user) {
            try {
                $user->password = app('hash')->make($request->password);
                $user->save();
            } catch (\Exception $exception) {
                return $this->errorResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
            }
            return $this->showData($user, Response::HTTP_CREATED, 'User Updated Success!');
        } else {
            return $this->errorResponse('User not found', Response::HTTP_NOT_FOUND);
        }
    }
}
