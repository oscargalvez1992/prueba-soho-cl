<?php

namespace App\Http\Repositories\User;

use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\User\V1\UserStoreRequest;

class UserStoreRepository extends UserRepository
{

    /**
     * @var $user
     */
    protected $user;

    /**
     * __construct
     *
     * @param User $user
     */
    public function __construct(User $user) {
        $this->user = $user;
    }


    /**
     * store
     * Store User
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function store(UserStoreRequest $request): JsonResponse
    {
        try {
            DB::transaction(function () use ($request) {
                $this->user->name = $request->name;
                $this->user->email = $request->email;
                $this->user->password = app('hash')->make($request->password);
                $this->user->save();
            }, 3);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->showData($this->user,Response::HTTP_CREATED);
    }
}
