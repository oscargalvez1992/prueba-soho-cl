<?php

namespace App\Http\Repositories\User;

use App\Http\Requests\User\V1\UserChangeStatusRequest;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;

class UserChangeStatusRepository extends UserRepository
{
    /**
     * @var $user
     */
    protected $user;

    /**
     * __construct
     *
     * @param User $user
     */
    public function __construct(User $user) {
        $this->user = $user;
    }

    /**
     * update
     *

     * @param UserUpdateRequest $request
     * @return JsonResponse
     */
    public function updateStatus(UserChangeStatusRequest $request) : JsonResponse
    {
        $user = $this->user::find($request->user_id);
        if ($user) {
            try {
                $user->status = $request->status;
                $user->save();
            } catch (\Exception $exception) {
                return $this->errorResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
            }
            return $this->showData($user, Response::HTTP_CREATED, 'User Updated Success!');
        } else {
            return $this->errorResponse('User not found', Response::HTTP_NOT_FOUND);
        }
    }
}
