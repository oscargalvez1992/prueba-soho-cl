<?php

namespace App\Http\Repositories\User;

use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;

class UserIndexRepository extends UserRepository
{

    /**
     * @var $user
     */
    protected $user;

    /**
     * __construct
     *
     * @param User $user
     */
    public function __construct(User $user) {
        $this->user = $user;
    }


    /**
     * index
     * List all User's
     * @return JsonResponse
     */
    public function index() : JsonResponse
    {
        $users = $this->user->all();
        if($users){
            return $this->showData($users,Response::HTTP_OK);
        }else{
            return $this->showData('there is no user in the database',Response::HTTP_NOT_FOUND);
        }
    }
}
