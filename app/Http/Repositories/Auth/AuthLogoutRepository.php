<?php

namespace App\Http\Repositories\Auth;

use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class AuthLogoutRepository extends AuthRepository
{
    /**
     * logout
     *
     * @return JsonResponse
     */
    public function logout() : JsonResponse
    {
        Auth::logout();
        return $this->showData(
            [
                'message' => "Successfully logged out",
            ],
            Response::HTTP_OK
        );
    }
}
