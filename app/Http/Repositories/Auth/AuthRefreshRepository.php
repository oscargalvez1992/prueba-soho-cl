<?php

namespace App\Http\Repositories\Auth;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class AuthRefreshRepository extends AuthRepository
{
    /**
     * refresh
     *
     * @return JsonResponse
     */
    public function refresh(): JsonResponse
    {
        return $this->respondWithToken(Auth::refresh());
    }
}
