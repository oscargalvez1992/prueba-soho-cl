<?php

namespace App\Http\Repositories\Auth;

use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class AuthLoginRepository extends AuthRepository
{
    /**
     * login
     *
     * @param $request
     * @return JsonResponse
     */
    public function login($request) : JsonResponse
    {
        $user = User::where('email','=',$request->email)->first();
        if($user->status){
            $credentials = $request->only('email', 'password');
            if (!$token = Auth::attempt($credentials)) {
                return $this->errorResponse('Unauthorized', Response::HTTP_UNAUTHORIZED);
            }
            return $this->respondWithToken($token);
        }else{
            return $this->errorResponse('User not active', Response::HTTP_UNAUTHORIZED);
        }
    }

}
