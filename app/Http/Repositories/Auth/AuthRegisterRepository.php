<?php

namespace App\Http\Repositories\Auth;

use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Auth\V1\RegisterRequest;

class AuthRegisterRepository extends AuthRepository
{
    /**
     * @var $authLoginRepository
     */
    protected $authLoginRepository;

    /**
     * @var $user
     */
    protected $user;

    /**
     * __construct
     *
     * @param AuthLoginRepository $authLoginRepository
     */
    public function __construct(AuthLoginRepository $authLoginRepository, User $user) {
        $this->authLoginRepository = $authLoginRepository;
        $this->user = $user;
    }

    /**
     * register
     *
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        try {
            DB::transaction(function () use ($request) {
                $this->user->name = $request->name;
                $this->user->email = $request->email;
                $this->user->password = app('hash')->make($request->password);
                $this->user->save();
            }, 3);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->authLoginRepository->login($request);
    }
}
